package com.stock.stockservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class QuarterDataStats {
    private Double growthAvg;
    private Double perShareGrowthAvg;
    private Double ltmGrowthAvg;
    private Double ltmPerShareGrowthAvg;
    private Double growthStd;
    private Double perShareGrowthStd;
    private Double ltmGrowthStd;
    private Double ltmPerShareGrowthStd;
}
