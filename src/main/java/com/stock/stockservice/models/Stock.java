package com.stock.stockservice.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Document("stock")
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class Stock {
    private @Id String symbol;
    private List<Quarter> quarters;
    private List<Price> prices;
    private Quarter lastQuarter;
    private Price lastPrice;
    private Quarter actualQuarter;
    private Price actualPrice;
}
