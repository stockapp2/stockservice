package com.stock.stockservice.models;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class Price {
    private Instant date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Double volume;
    // Calculated
    private Double per;
    private Double ev;
    private Double marketCap;
    private PriceData evData;
    private PriceData marketCapData;
    // Stats
    private ValuePriceData closeData;
    private ValuePriceData volumeData;
    // Est
    private FuturePriceGrowth estNextY;
    private FuturePriceGrowth estNext3Y;
    private FuturePriceGrowth estNext5Y;
}
