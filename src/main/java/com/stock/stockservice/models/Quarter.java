package com.stock.stockservice.models;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class Quarter {
    private Instant earningDate;
    private Instant nextEarningDate;
    private Instant fiscalDateEnd;
    // Income Statement
    private Double revenues;
    private Double costsOfGoodsSold;
    private Double grossProfit;
    private Double operatingExpenses;
    private Double operatingIncome;
    private Double netIncome;
    private Double shares;
    // Balance Sheet
    private Double totalCashAndShortInv;
    private Double totalReceivable;
    private Double totalCurrentAssets;
    private Double netPropertyPlantAndEquip;
    private Double goodwill;
    private Double totalAssets;
    private Double totalCurrentLiabilities;
    private Double longTermDebt;
    private Double totalLiabilities;
    private Double totalEquity;
    private Double netDebt;
    // Cash Flow
    private Double totalDeprecAmort;
    private Double stockBasedCompensation;
    private Double cashFromOperations;
    private Double capitalExpenditure;
    private Double cashFromInvesting;
    private Double cashFromFinancing;
    private Double netChangeinCash;
    //Calculated
    private Double eps;
    private Double ebit;
    private Double ebitda;
    private Double freeCashFlow;
    private Double currentRatio;
    private Double roi;
    private Double roe;
    private Double roa;
    // Margins
    private Double grossProfitMargin;
    private Double operatingIncomeMargin;
    private Double netIncomeMargin;
    private Double ebitMargin;
    private Double ebitdaMargin;
    private Double freeCashFlowMargin;
    // Data
    private QuarterData revenuesData;
    private QuarterData grossProfitData;
    private QuarterData operatingIncomeData;
    private QuarterData netIncomeData;
    private QuarterData epsData;
    private QuarterData ebitdaData;
    private QuarterData freeCashFlowData;
    private QuarterData sharesData;
}
