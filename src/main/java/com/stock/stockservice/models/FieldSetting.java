package com.stock.stockservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class FieldSetting {
    private String field;
    private Integer valueMax;
    private Integer valueMin;
    private Comparator comparator;

    public enum Comparator {
        MORE_THAN("MORE_THAN"),
        LESS_THAN("LESS_THAN"),
        BETWEEN("BETWEEN"),
        EQUAL("EQUAL");

        private final String value;

        Comparator(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
