package com.stock.stockservice.models.googledocs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class QuarterDocs {
    private String earningDate;
    private String fiscalDateEnd;
    // Income Statement
    private String revenues;
    private String costsOfGoodsSold;
    private String grossProfit;
    private String operatingExpenses;
    private String operatingIncome;
    private String netIncome;
    private String shares;
    // Balance Sheet
    private String totalCashAndShortInv;
    private String totalReceivable;
    private String totalCurrentAssets;
    private String netPropertyPlantAndEquip;
    private String goodwill;
    private String totalAssets;
    private String totalCurrentLiabilities;
    private String longTermDebt;
    private String totalLiabilities;
    private String totalEquity;
    private String netDebt;
    // Cash Flow
    private String totalDeprecAmort;
    private String stockBasedCompensation;
    private String cashFromOperations;
    private String capitalExpenditure;
    private String cashFromInvesting;
    private String cashFromFinancing;
    private String netChangeinCash;
    
}
