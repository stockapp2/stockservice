package com.stock.stockservice.models.googledocs;

public enum QuarterDocValues {
    EARNING_DATE("Earning Date"),
    FISCAL_DATE("Income Statement"),
    REVENUES("Total Revenues"),
    COST_OF_GOODS_SOLD("Cost of Goods Sold"),
    GROSS_PROFIT("Gross Profit"),
    OPERATING_EXPENSES("Other Operating Expenses"),
    OPERATING_INCOME("Operating Income"),
    NET_INCOME("Net Income"),
    SHARES("Weighted Average Diluted Shares Outstanding"),
    TOTAL_CASH_AND_SHORT_TERM_INVESTMENTS("Total Cash And Short Term Investments"),
    TOTAL_RECEIVABLES("Total Receivables"),
    TOTAL_CURRENT_ASSETS("Total Current Assets"),
    NET_PROPERTY_PLANT_AND_EQUIPMENT("Net Property Plant And Equipment"),
    GOODWILL("Goodwill"),
    TOTAL_ASSETS("Total Assets"),
    TOTAL_CURRENT_LIABILITIES("Total Current Liabilities"),
    LONG_TERM_DEBT("Long-Term Debt"),
    TOTAL_LIABILITIES("Total Liabilities"),
    TOTAL_EQUITY("Total Equity"),
    NET_DEBT("Net Debt"),
    DEPRECIATION_AND_AMORTIZATION("Total Depreciation & Amortization"),
    STOCK_BASED_COMPENSATION("Stock-Based Compensation"),
    CASH_FROM_OPERATIONS("Cash from Operations"),
    CAPITAL_EXPENDITURE("Capital Expenditure"),
    CASH_FROM_INVESTING("Cash from Investing"),
    CASH_FROM_FINANCING("Cash from Financing"),
    NET_CHANGE_IN_CASH("Net Change in Cash")
    ;

    private final String text;

    QuarterDocValues(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
