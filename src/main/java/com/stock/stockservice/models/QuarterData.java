package com.stock.stockservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class QuarterData {
    private Double value;
    private Double growth;
    private Double perShare;
    private Double perShareGrowth;
    private Double ltm;
    private Double ltmGrowth;
    private Double ltmPerShare;
    private Double ltmPerShareGrowth;
    private Double ltmPerShareGrowthAvg;
    private QuarterDataStats fullStats;
    private QuarterDataStats lastYStats;
    private QuarterDataStats last3yStats;
    private QuarterDataStats last5yStats;
}
