package com.stock.stockservice.models.tikr;

public enum QuarterField {
    EARNING_DATE {
        public Integer id() {
            return -1;
        }
    },
    FISCAL_DATE_END {
        public Integer id() {
            return -1;
        }
    },
    REVENUES {
        public Integer id() {
            return 112;
        }
    },
    COSTS_OF_GOOD_SOLD {
        public Integer id() {
            return 34;
        }
    },
    GROSS_PROFIT {
        public Integer id() {
            return 10;
        }
    },
    OPERATING_EXPENSES {
        public Integer id() {
            return 380;
        }
    },
    OPERATING_INCOME {
        public Integer id() {
            return 21;
        }
    },
    NET_INCOME {
        public Integer id() {
            return 41571;
        }
    },
    SHARES {
        public Integer id() {
            return 342;
        }
    },
    TOTAL_CASH_AND_SHORT_INVESTMENTS {
        public Integer id() {
            return 1002;
        }
    },
    TOTAL_RECEIVABLES {
        public Integer id() {
            return 1001;
        }
    },
    TOTAL_CURRENT_ASSETS {
        public Integer id() {
            return 1008;
        }
    },
    NET_PROPERTY_PLANT_AND_EQUIPMENT {
        public Integer id() {
            return 1004;
        }
    },
    GOODWILL {
        public Integer id() {
            return 1171;
        }
    },
    TOTAL_ASSETS {
        public Integer id() {
            return 1013;
        }
    },
    TOTAL_CURRENT_LIABILITIES {
        public Integer id() {
            return 1009;
        }
    },
    LONG_TERM_DEBT {
        public Integer id() {
            return 1049;
        }
    },
    TOTAL_LIABILITIES {
        public Integer id() {
            return 1276;
        }
    },
    TOTAL_EQUITY {
        public Integer id() {
            return 1275;
        }
    },
    NET_DEBT {
        public Integer id() {
            return 4364;
        }
    },
    TOTAL_DEPRECIATION_AND_AMORTIZATION {
        public Integer id() {
            return 2171;
        }
    },
    STOCK_BASED_COMPENSATION {
        public Integer id() {
            return 2127;
        }
    },
    CASH_FROM_OPERATIONS {
        public Integer id() {
            return 2006;
        }
    },
    CAPITAL_EXPENDITURE {
        public Integer id() {
            return 2021;
        }
    },
    CASH_FROM_INVESTING {
        public Integer id() {
            return 2005;
        }
    },
    CASH_FROM_FINANCING {
        public Integer id() {
            return 2004;
        }
    },
    NET_CHANGE_IN_CASH {
        public Integer id() {
            return 2093;
        }
    },
    EPS {
        public Integer id() {
            return -1;
        }
    },
    EBIT {
        public Integer id() {
            return -1;
        }
    },
    EBITDA {
        public Integer id() {
            return -1;
        }
    };

    public abstract Integer id();
}
