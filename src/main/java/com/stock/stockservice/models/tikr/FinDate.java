package com.stock.stockservice.models.tikr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class FinDate {
    public Integer financialperiodid;
    public Integer companyid;
    public Integer periodtypeid;
    public Integer calendaryear;
    public Integer calendarquarter;
    public Integer fiscalyear;
    public Integer fiscalquarter;
    public Integer latestperiodflag;
    public Integer fiscalchainseriesid;
    public String fiperiodenddate;
    public String fifilingdate;
    public Integer currencyid;
    public String pricedate;
    public Integer snapid;
    public String priceclose;
    public String bestpricedate;
    public Integer latestsnapflag;
    public Integer carryforwardflag;
    public String currencyname;
    public String isocode;
    public Integer countryid;
    public Integer majorcurrencyflag;
}
