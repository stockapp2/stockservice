package com.stock.stockservice.models.tikr;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class FinancialPeriod {
    public FinDate date;
    public List<FinData> data;
    public List<FinRpt> rpt;
    public List<FinStd> std;
}
