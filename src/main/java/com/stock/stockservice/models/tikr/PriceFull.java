package com.stock.stockservice.models.tikr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class PriceFull {
    public Integer numPrice;
    public List<PriceData> price;
    public List<PriceCompany> company;
    public List<PriceFin> fin;
    public PriceData last;
}
