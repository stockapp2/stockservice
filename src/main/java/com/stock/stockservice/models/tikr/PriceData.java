package com.stock.stockservice.models.tikr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class PriceData {
    public String a;
    public String c;
    public String h;
    public String l;
    public String m;
    public String o;
    public String d;
    public String v;
    public String pc;
}
