package com.stock.stockservice.models.tikr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class PriceCompany {
    public String pricingdate;
    public String marketcap;
    public String tev;
    public String sharesoutstanding;
    public String tickersymbol;
    public Integer exchangeid;
    public Integer currencyid;
    public String currencyname;
    public Integer countryid;
    public String isocode;
    public String priceclose;
}
