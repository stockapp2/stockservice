package com.stock.stockservice.models.tikr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class PriceFin {
    public String dataitemname;
    public Integer datacollectiontypeid;
    public Integer financialcollectionid;
    public Integer dataitemid;
    public String dataitemvalue;
    public Integer unittypeid;
    public Integer nmflag;
    public Integer pacvertofeedpop;
    public String formtype;
    public Integer financialperiodid;
    public Integer calendaryear;
    public Integer calendarquarter;
    public Integer fiscalyear;
    public Integer fiscalquarter;
}
