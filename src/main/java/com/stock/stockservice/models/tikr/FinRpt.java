package com.stock.stockservice.models.tikr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class FinRpt {
    public Integer financialperiodid;
    public Integer calendaryear;
    public Integer calendarquarter;
    public Integer segmenttypeid;
    public String segmentname;
    public String dataitemname;
    public Integer dataitemid;
    public String dataitemvalue;
    public Integer unittypeid;
    public Integer maintablelinenumber;
    public Boolean unauth;
}
