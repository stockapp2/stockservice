package com.stock.stockservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class ValuePriceData {
    private Double value;
    private Double avg;
    private PriceDataStats fullStats;
    private PriceDataStats lastQStats;
    private PriceDataStats lastYStats;
    private PriceDataStats last3yStats;
    private PriceDataStats last5yStats;
    private PriceDataStats last10yStats;
    private PriceEstimation estNextY;
    private PriceEstimation estNext3Y;
    private PriceEstimation estNext5Y;
}
