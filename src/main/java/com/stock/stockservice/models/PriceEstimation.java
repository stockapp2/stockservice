package com.stock.stockservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class PriceEstimation {
    private Double fairValue;
    private Double fairValueGrowth;
    private Double futurePrice;
    private Double futurePriceGrowth;
    private Double cagr;
}
