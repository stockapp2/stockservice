package com.stock.stockservice.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class PriceData {
    private Double value;
    private Double fcfYield;
    // Value Price data
    private ValuePriceData revenuesLtm;
    private ValuePriceData grossProfitLtm;
    private ValuePriceData operatingIncomeLtm;
    private ValuePriceData netIncomeLtm;
    private ValuePriceData epsLtm;
    private ValuePriceData ebitdaLtm;
    private ValuePriceData freeCashFlowLtm;
    // Est
    private FuturePriceGrowth estNextY;
    private FuturePriceGrowth estNext3Y;
    private FuturePriceGrowth estNext5Y;
}
