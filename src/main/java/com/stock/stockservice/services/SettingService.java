package com.stock.stockservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.stockservice.models.Setting;
import com.stock.stockservice.repositories.SettingMongoRepository;

import reactor.core.publisher.Mono;

@Service
public class SettingService {

    private static final String UNIQUE_SETTING_ID = "unique_setting_id";

    @Autowired
    private SettingMongoRepository settingRepository;

    public Mono<Setting> getSettings() {
        return settingRepository.findById(UNIQUE_SETTING_ID)
                .map(Mono::just)
                .orElse(Mono.empty());
    }

    public Setting saveSettings(Setting setting) {
        setting.setId(UNIQUE_SETTING_ID); // Ensure only one setting document
        return settingRepository.save(setting);
    }
}
