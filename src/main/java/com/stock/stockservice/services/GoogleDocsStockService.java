package com.stock.stockservice.services;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.stockservice.models.Quarter;
import com.stock.stockservice.models.googledocs.QuarterDocValues;
import com.stock.stockservice.models.googledocs.QuarterDocs;
import com.stock.stockservice.utils.QuarterUtils;

@Service
public class GoogleDocsStockService {

    @Autowired
    GoogleDocsService googleDocsService;

    public List<String> quarterValuesToMap = new ArrayList<>(Arrays.asList(QuarterDocValues.values()).stream().map(QuarterDocValues::toString).toList());

     public Map<String, String> symbolToIdMap = new HashMap<String, String>() {
            {
                put("AMZN", "1lYKuS4COsoVw2ocOFsQp_89lqQ3dDIKpw4kxF9QfNPs");
                put("GOOGL", "1ovU91-AA0DX_ymktZibIjj1sE0ErxzwYGcsIT99dcQE");
                put("META", "1kL21v6VTbGFBLLQKmritoie3-MIW97WwfhKDQmIE9Cg");
                put("TSLA", "12jvT3IDiFTFp1TZVqCHdLR49XZAlnEutQXSK1yBryeg");
                put("NVDA", "1aISPkXJUBotcdeY7UMTiA9rUciRs8dFLF5OC2nzjK9M");
                put("BABA", "1sCbbXBIyGPi5k92xlCaXEOPZUW1UoSFo_8UTfNgNSps");
                put("FICO", "1BCsvTBjqHzOQUsyoNKTGRlBaXHnHORc6rvrsmQYiIzc");
                put("U", "1jyj2i8NHlTzNY7HzllIdAotMjSLu123gBMpuJ485Zu8");
                put("V", "1YdlLRg-5ByEoK2QeR8lJK_KVgx6y7ZpnrG9ZvTJev-8");
            }
        };

    public List<Quarter> processFile(String symbol) {
        final String spreadsheetId = symbolToIdMap.get(symbol);
        List<QuarterDocs> quarterDocs = new ArrayList<>();
        List<List<Object>> values = processTabs(spreadsheetId, quarterValuesToMap);
      
        if(areValidValues(values)) {
            quarterDocs = Stream.generate(QuarterDocs::new).limit(values.get(0).size()).toList();
            for (int i = 0; i < values.size(); i++) {
                List<Object> valueList = values.get(i);
                for (int j = 1; j < valueList.size(); j++) {
                    QuarterDocValues incomeDocValues = stringToEnum((String) valueList.get(0));
                    setQuarterValue(quarterDocs.get(j), incomeDocValues, (String) valueList.get(j));
                }
            }
        }
        List<Quarter> quarters = quarterDocs.stream().filter(q -> q.getFiscalDateEnd() != null).map(this::mapToQuarter).toList();
        // return quarters;
        return QuarterUtils.processQuarters(quarters);
    }

    public List<List<Object>> processTab(String spreadsheetId, String range, List<String> valuesToMap) {
        List<List<Object>> values = googleDocsService.processFile(spreadsheetId, range);
        List<List<Object>> valuesFiltered = new ArrayList<>();
        if(areValidValues(values)) {
            for (List<Object> row : values) {
                if(!row.isEmpty() && row.size() > 0 && valuesToMap.contains(row.get(0))) {
                    valuesFiltered.add(row);
                }
            }
        }
        return valuesFiltered;
    }

    public List<List<Object>> processTabs(String spreadsheetId, List<String> valuesToMap) {
        List<List<Object>> values = new ArrayList<>();
        List<List<Object>> incomeTabValues = this.processTab(spreadsheetId, "Income", valuesToMap);
        List<List<Object>> balanceTabValues = this.processTab(spreadsheetId, "Balance", valuesToMap);
        List<List<Object>> cashFlowTabValues = this.processTab(spreadsheetId, "CashFlow", valuesToMap);
        values.addAll(incomeTabValues);
        values.addAll(balanceTabValues);
        values.addAll(cashFlowTabValues);
        return values;
    }

    public Quarter mapToQuarter(QuarterDocs quarterDocs) {
        Instant earningDate = toInstant(quarterDocs.getEarningDate());
        Quarter q = Quarter.builder()
                .earningDate(earningDate)
                .nextEarningDate(earningDate.plus(90, ChronoUnit.DAYS))
                .fiscalDateEnd(toInstant(quarterDocs.getFiscalDateEnd()))
                .revenues(toDouble(quarterDocs.getRevenues()))
                .costsOfGoodsSold(toDouble(quarterDocs.getCostsOfGoodsSold()))
                .grossProfit(toDouble(quarterDocs.getGrossProfit()))
                .operatingExpenses(toDouble(quarterDocs.getOperatingExpenses()))
                .operatingIncome(toDouble(quarterDocs.getOperatingIncome()))
                .netIncome(toDouble(quarterDocs.getNetIncome()))
                .shares(toDouble(quarterDocs.getShares()))
                .totalCashAndShortInv(toDouble(quarterDocs.getTotalCashAndShortInv()))
                .totalReceivable(toDouble(quarterDocs.getTotalReceivable()))
                .totalCurrentAssets(toDouble(quarterDocs.getTotalCurrentAssets()))
                .netPropertyPlantAndEquip(toDouble(quarterDocs.getNetPropertyPlantAndEquip()))
                .goodwill(toDouble(quarterDocs.getGoodwill()))
                .totalAssets(toDouble(quarterDocs.getTotalAssets()))
                .totalCurrentLiabilities(toDouble(quarterDocs.getTotalCurrentLiabilities()))
                .longTermDebt(toDouble(quarterDocs.getLongTermDebt()))
                .totalLiabilities(toDouble(quarterDocs.getTotalLiabilities()))
                .totalEquity(toDouble(quarterDocs.getTotalEquity()))
                .netDebt(toDouble(quarterDocs.getNetDebt()))
                .totalDeprecAmort(toDouble(quarterDocs.getTotalDeprecAmort()))
                .stockBasedCompensation(toDouble(quarterDocs.getStockBasedCompensation()))
                .cashFromOperations(toDouble(quarterDocs.getCashFromOperations()))
                .capitalExpenditure(toDouble(quarterDocs.getCapitalExpenditure()))
                .cashFromInvesting(toDouble(quarterDocs.getCashFromInvesting()))
                .cashFromFinancing(toDouble(quarterDocs.getCashFromFinancing()))
                .netChangeinCash(toDouble(quarterDocs.getNetChangeinCash()))
                .build();
        return QuarterUtils.setCalculatedValues(q);
    }

    private Instant toInstant(String dateString) {
        try {
            // Define the date format pattern
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yy");

            // Parse the date string to a LocalDate
            LocalDate localDate = LocalDate.parse(dateString, formatter);

            // Convert the LocalDate to an Instant
            Instant instant = localDate.atStartOfDay(ZoneOffset.UTC).toInstant();
            return instant;
        } catch (Exception e) {
            // Handle parsing errors here
            return null;
        }
    }

    public Double toDouble(String input) {
        if (input == null || input.isEmpty()) {
            return null;
        }
    
        // Remove commas and other non-numeric characters
        String cleanedInput = input.replaceAll("[^0-9.,()-]", "");
    
        // Replace commas with periods to ensure a valid numeric format
        cleanedInput = cleanedInput.replace(",", "");
    
        try {
            // Check if the number is negative and remove parentheses
            if (cleanedInput.startsWith("(") && cleanedInput.endsWith(")")) {
                cleanedInput = "-" + cleanedInput.substring(1, cleanedInput.length() - 1);
            }
    
            return Double.parseDouble(cleanedInput);
        } catch (NumberFormatException e) {
            return null; // Handle conversion errors as needed
        }
    }

    public QuarterDocs setQuarterValue(QuarterDocs q, QuarterDocValues header, String value) {
        switch(header) {
             case EARNING_DATE:
                q.setEarningDate(value);
                break;
            case FISCAL_DATE:
                q.setFiscalDateEnd(value);
                break;
            case REVENUES:
                q.setRevenues(value);
                break;
            case COST_OF_GOODS_SOLD:
                q.setCostsOfGoodsSold(value);
                break;
            case GROSS_PROFIT:
                q.setGrossProfit(value);
                break;
            case OPERATING_EXPENSES:
                q.setOperatingExpenses(value);
                break;
            case OPERATING_INCOME:
                q.setOperatingIncome(value);
                break;
            case NET_INCOME:
                q.setNetIncome(value);
                break;
            case SHARES:
                q.setShares(value);
                break;
            case TOTAL_CASH_AND_SHORT_TERM_INVESTMENTS:
                q.setTotalCashAndShortInv(value);
                break;
            case TOTAL_RECEIVABLES:
                q.setTotalReceivable(value);
                break;
            case TOTAL_CURRENT_ASSETS:
                q.setTotalCurrentAssets(value);
                break;
            case NET_PROPERTY_PLANT_AND_EQUIPMENT:
                q.setNetPropertyPlantAndEquip(value);
                break;
            case GOODWILL:
                q.setGoodwill(value);
                break;
            case TOTAL_ASSETS:
                q.setTotalAssets(value);
                break;
            case TOTAL_CURRENT_LIABILITIES:
                q.setTotalCurrentLiabilities(value);
                break;
            case LONG_TERM_DEBT:
                q.setLongTermDebt(value);
                break;
            case TOTAL_LIABILITIES:
                q.setTotalLiabilities(value);
                break;
            case TOTAL_EQUITY:
                q.setTotalEquity(value);
                break;
            case NET_DEBT:
                q.setNetDebt(value);
                break;
            case DEPRECIATION_AND_AMORTIZATION:
                q.setTotalDeprecAmort(value);
                break;
            case STOCK_BASED_COMPENSATION:
                q.setStockBasedCompensation(value);
                break;
            case CASH_FROM_OPERATIONS:
                q.setCashFromOperations(value);
                break;
            case CAPITAL_EXPENDITURE:
                q.setCapitalExpenditure(value);
                break;
            case CASH_FROM_INVESTING:
                q.setCashFromInvesting(value);
                break;
            case CASH_FROM_FINANCING:
                q.setCashFromFinancing(value);
                break;
            case NET_CHANGE_IN_CASH:
                q.setNetChangeinCash(value);
                break;
            default:
                break;
        }
        return q;
    }

    private static QuarterDocValues stringToEnum(String input) {
        for (QuarterDocValues enumValue : QuarterDocValues.values()) {
            if (enumValue.toString().equals(input)) {
                return enumValue;
            }
        }
        return null;
    }

    public boolean areValidValues(List<List<Object>> values) {
        return values != null && !values.isEmpty();
    }
    
}
