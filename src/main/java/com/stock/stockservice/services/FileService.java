package com.stock.stockservice.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

@Service
public class FileService {

     public List<String> readFile(String fileName) throws FileNotFoundException, IOException {
        List<String> records = new ArrayList<>();
        InputStream resource = new ClassPathResource("/"+ fileName +".csv").getInputStream();
          try ( BufferedReader reader = new BufferedReader(
            new InputStreamReader(resource)) ) {
              records = reader.lines().collect(Collectors.toList());
          }
        return records;
    }

    public void createFoldersAndFilesFromStocksFile(String path) {
        try {
            List<String> symbols = readFile("stocks");
            createFolderAndFilesSymbolList(symbols, path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createFolderAndFilesSymbolList(List<String> symbols, String path) {
         symbols.forEach(s -> {
            createFolderAndFilesSymbol(s, path);
        });
    }

    private void createFolderAndFilesSymbol(String s, String path) {
        String dir = createDirectory(s, path);
        createJsonFile(dir, "fin");
        createJsonFile(dir, "price");
    }

     private String createDirectory(String symbol, String path) {
        File csvDirectory = new File(path.concat(symbol));
        if (!(csvDirectory.exists())) {
            csvDirectory.mkdir();
        }
        return csvDirectory.getPath();
    }

     private File createJsonFile(String filePath, String fileName) {
        File file = new File(filePath + "/" + fileName + ".json");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;


    }



    
}
