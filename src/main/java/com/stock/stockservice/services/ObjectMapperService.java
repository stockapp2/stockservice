package com.stock.stockservice.services;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Service
public class ObjectMapperService {

    private ObjectMapper redisObjectMapper;

    public ObjectMapperService() {
        redisObjectMapper = JsonMapper.builder()
        .addModule(new JavaTimeModule())
        .build();
        redisObjectMapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);
        redisObjectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public ObjectMapper getRedisObjectMapper(){
        return redisObjectMapper;
    }
}