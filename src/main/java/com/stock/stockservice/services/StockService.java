package com.stock.stockservice.services;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.stock.stockservice.models.Price;
import com.stock.stockservice.models.Quarter;
import com.stock.stockservice.models.Stock;
import com.stock.stockservice.repositories.StockMongoRepository;
import com.stock.stockservice.utils.PricesUtils;
import com.stock.stockservice.utils.Utils;

@Service
public class StockService {

    @Autowired
    TikrFileService stockFileService;

    @Autowired
    PriceEstimationService priceEstimationService;

    @Autowired
    GoogleDocsStockService googleDocsStockService;

    @Autowired
    StockPriceService stockPriceService;

    @Autowired
    StockMongoRepository stockMongoRepository;

    public List<Stock> processAndSaveStocks(List<String> symbols) {
        return stockMongoRepository.saveAll(symbols.stream().map(s -> processStock(s)).toList());
    }

    public Stock processStock(String symbol) {
        System.out.println(symbol);
        Stock stock = Stock.builder().symbol(symbol).build();
        stock.setQuarters(googleDocsStockService.processFile(symbol));
        stock.setLastQuarter(Utils.getLastQuarter(stock));
        stock = stockFileService.processFilePrice(symbol, stock);
        stock = priceEstimationService.calculateEstPriceFull(stock);
        return stock;
    }

    public Stock getStock(String symbol, Instant date) {
        return calculateStock(stockMongoRepository.findBySymbol(symbol), date);
    }

    public List<Stock> getStocks(List<String> symbols, Instant date) {
        List<Stock> stocks;
        Boolean isSameDay = DateService.isSameDay(date);
        if (CollectionUtils.isEmpty(symbols)) {
            stocks = isSameDay ? stockMongoRepository.findAllWithoutQuartersAndPrices() : stockMongoRepository.findAll();
        } else {
            stocks = isSameDay ? stockMongoRepository.findBySymbolList(symbols) : stockMongoRepository.findBySymbolIn(symbols);
        }
        if (date != null) {
            return stocks.stream().map(s -> calculateStock(s, date)).toList();
        }
        return stocks;
    }

    public Stock calculateStock(Stock stock, Instant date) {
        List<Quarter> quarters = stock.getQuarters();
        List<Price> prices = stock.getPrices();
        // Instant lastPriceDate = stockPriceService.getDate();
    
        if (quarters != null && !quarters.isEmpty()) {
            stock.setLastQuarter(Utils.getLastQuarter(stock));
            stock.setQuarters(Utils.sortAndFilterQuartersByDate(quarters, date));
            stock.setActualQuarter(Utils.getLastQuarter(stock.getQuarters()));
        } else {
            stock.setActualQuarter(stock.getLastQuarter());
        }

        if (prices != null && !prices.isEmpty()) {
            stock.setLastPrice(Utils.getLastPrice(prices));
            stock.setPrices(Utils.filterPricesByDate(prices, date));
            stock.setActualPrice(Utils.getLastPrice(stock.getPrices()));
        } else {
            stock.setActualPrice(stock.getLastPrice());
        }
/*         if (lastPriceDate.isAfter(stock.getLastPrice().getDate())) {
            Double lastPriceValue = stockPriceService.getPrice(stock.getSymbol());
            if (lastPriceValue != null) {
                Price lastPrice = PricesUtils.buildAndProcessPrice(lastPriceValue, lastPriceDate, stock);
                stock.setLastPrice(lastPrice);
                stock.setActualPrice(stock.getLastPrice());
            }
        } */
        stock = priceEstimationService.calculateEstPrice(stock);
        return stock;
    }

}
