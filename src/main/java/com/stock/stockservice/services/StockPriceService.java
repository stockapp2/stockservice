package com.stock.stockservice.services;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.stockservice.models.StockPrices;
import com.stock.stockservice.repositories.StockPriceMongoRepository;

import reactor.core.publisher.Mono;

@Service
public class StockPriceService {

    private static final String UNIQUE_SETTING_ID = "unique_setting_id";

    @Autowired
    private StockPriceMongoRepository stockPriceMongoRepository;

    private StockPrices stockPrices;
    private Instant date;

    public Mono<StockPrices> getStockPrices() {
        return stockPriceMongoRepository.findById(UNIQUE_SETTING_ID)
                .map(Mono::just)
                .orElse(Mono.empty());
    }

    public void cacheStockPrice() {
        this.stockPrices = stockPriceMongoRepository.findById(UNIQUE_SETTING_ID).get();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
        try {
            LocalDate localDate = LocalDate.parse(stockPrices.getDate(), formatter);
            Instant instant = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
            this.date = instant;
        } catch (DateTimeParseException e) {
            System.err.println("Invalid date format: " + stockPrices.getDate());
            e.printStackTrace();
        }
    }

    public Double getPrice(String symbol) {
        return stockPrices.getPrices().get(symbol);
    }

    public Instant getDate() {
        return this.date;
    }

    public StockPrices saveStockPrices(StockPrices stockPrices) {
        stockPrices.setId(UNIQUE_SETTING_ID); // Ensure only one setting document
        StockPrices stockPricesSaved = stockPriceMongoRepository.save(stockPrices);
        this.cacheStockPrice();
        return stockPricesSaved;
    }
}
