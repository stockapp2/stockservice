package com.stock.stockservice.services;

import java.lang.reflect.Field;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.stock.stockservice.models.FuturePriceGrowth;
import com.stock.stockservice.models.Price;
import com.stock.stockservice.models.PriceData;
import com.stock.stockservice.models.PriceEstimation;
import com.stock.stockservice.models.Quarter;
import com.stock.stockservice.models.Stock;
import com.stock.stockservice.models.ValuePriceData;
import com.stock.stockservice.utils.Utils;

@Service
public class PriceEstimationService {

    public Stock calculateEstPrice(Stock stock) {
        calculateQuarterAvgs(stock.getActualQuarter());
        calculatePriceAvgsAndEst(stock.getActualPrice(), stock.getActualQuarter());
        return stock;
    }

    public Stock calculateEstPriceFull(Stock stock) {
        List<Price> prices = Utils.sortPrices(stock.getPrices());
        Quarter firstQuarter = stock.getQuarters().get(0);
        prices = Utils.filterPricesByRange(prices, firstQuarter.getEarningDate().minus(100, ChronoUnit.DAYS), Instant.now());
        for (int i = 0; i < stock.getQuarters().size(); i++) {
            calculateQuarterAvgs(stock.getQuarters().get(i));
        }
        for (int i = 0; i < prices.size(); i++) {
            Price p = prices.get(i);
            Quarter q = Utils.getLastQuarterByDate(stock.getQuarters(), p.getDate());
            calculatePriceAvgsAndEst(p, q);
        }
        return stock;
    }

    public void calculatePriceAvgsAndEst(Price price, Quarter quarter) {
        List<String> priceFields = Arrays.asList("evData", "marketCapData");
        List<String> priceDataFields = Arrays.asList("revenuesLtm", "grossProfitLtm", "operatingIncomeLtm",
                                                     "netIncomeLtm", "epsLtm", "ebitdaLtm", "freeCashFlowLtm");

        List<String> statsFields = Arrays.asList("lastYStats", "last3yStats", "last5yStats");
        List<Integer> yearsToProjectList = Arrays.asList(1, 3, 5);
        List<Double> statsValues;
        double metricGrowthRate;
        double ratio;
        double actualRatio;
        double priceClose = price.getClose();
        Object estPrice;

        for (String f : priceFields) {
            for (String df : priceDataFields) {
                PriceData priceData = (PriceData) getFieldValue(price, f);
                if (priceData != null) {
                    ValuePriceData valuePriceData = (ValuePriceData) getFieldValue(priceData, df);
                    if (valuePriceData != null) {
                        statsValues = statsFields.stream()
                                                 .map(v -> getNestedValue(valuePriceData, v + ".avg"))
                                                 .filter(Objects::nonNull)
                                                 .mapToDouble(Double::doubleValue)
                                                 .boxed()
                                                 .collect(Collectors.toList());
                        ratio = mean(statsValues);
                        setNestedValue(valuePriceData, "avg", ratio);
                        metricGrowthRate = (double) getNestedValue(quarter, df.replace("Ltm", "Data") + ".ltmPerShareGrowthAvg");

                        if (metricGrowthRate != 0) {
                            actualRatio = (double) getFieldValue(valuePriceData, "value");
                            for (int y : yearsToProjectList) {
                                estPrice = calculateEstPriceField(priceClose, actualRatio, ratio, metricGrowthRate, y);
                                setNestedValue(valuePriceData, y == 1 ? "estNextY" : "estNext" + y + "Y", estPrice);
                            }
                        }
                    }
                }
            }
        }

        for (int y : yearsToProjectList) {
            String estField = y == 1 ? "estNextY" : "estNext" + y + "Y";
            List<Double> estPriceList = priceFields.stream()
                                                   .map(f -> (PriceData) getFieldValue(price, f))
                                                   .filter(Objects::nonNull)
                                                   .flatMap(pd -> priceDataFields.stream()
                                                                                 .map(df -> (ValuePriceData) getFieldValue(pd, df))
                                                                                 .filter(Objects::nonNull)
                                                                                 .map(vpd -> (PriceEstimation) getFieldValue(vpd, estField)))
                                                   .filter(Objects::nonNull)
                                                   .map(PriceEstimation::getFuturePrice)
                                                   .collect(Collectors.toList());
            double estPriceValue = mean(estPriceList);
            setFieldValue(price, estField + "List", estPriceList);
            setFieldValue(price, estField, calculateFuturePriceGrowth(priceClose, estPriceValue, y));
        }
    }

    public Object calculateEstPriceField(double priceClose, double actualRatio, double ratio, double metricGrowthRate, int yearsToProject) {
        double fairValue = getNewValue(priceClose, actualRatio, ratio);
        double fairValueGrowth = getPercentage(fairValue, priceClose);
        double futurePrice = applyCagr(fairValue, metricGrowthRate, yearsToProject);
        double futurePriceGrowth = getPercentage(futurePrice, priceClose);
        double cagr = calculateCagr(priceClose, futurePrice, yearsToProject);

        return new PriceEstimation(fairValue, fairValueGrowth, futurePrice, futurePriceGrowth, cagr);
    }

    public Object calculateFuturePriceGrowth(double priceClose, double futurePrice, int yearsToProject) {
        double futurePriceGrowth = getPercentage(futurePrice, priceClose);
        double cagr = calculateCagr(priceClose, futurePrice, yearsToProject);
        return new FuturePriceGrowth(futurePrice, futurePriceGrowth, cagr);
    }

    public void calculateQuarterAvgs(Quarter quarter) {
        List<String> statsFields = Arrays.asList("lastYStats", "last3yStats", "last5yStats");
        List<Double> statsValues;

        List<String> quarterDataFields = Arrays.asList("revenuesData", "grossProfitData", "operatingIncomeData",
                                                       "netIncomeData", "epsData", "ebitdaData", "freeCashFlowData", "sharesData");

        for (String f : quarterDataFields) {
            statsValues = statsFields.stream()
                                     .map(v -> getNestedValue(quarter, f + "." + v + ".ltmPerShareGrowthAvg"))
                                     .filter(Objects::nonNull)
                                     .mapToDouble(Double::doubleValue)
                                     .boxed()
                                     .collect(Collectors.toList());
            setNestedValue(quarter, f + ".ltmPerShareGrowthAvg", mean(statsValues));
        }
    }

    // Helper methods converted to Java
    private Double getNestedValue(Object object, String fieldPath) {
        String[] keys = fieldPath.split("\\.");
        Object result = object;
        for (String key : keys) {
            result = getFieldValue(result, key);
            if (result == null) {
                return null;
            }
        }
        return result instanceof Double ? (Double) result : null;
    }

    private void setNestedValue(Object object, String fieldPath, Object value) {
        String[] keys = fieldPath.split("\\.");
        Object result = object;
        for (int i = 0; i < keys.length - 1; i++) {
            result = getFieldValue(result, keys[i]);
            if (result == null) {
                return;
            }
        }
        setFieldValue(result, keys[keys.length - 1], value);
    }

       private Object getFieldValue(Object object, String fieldName) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(object);
        } catch (Exception e) {
            return null;
        }
    }

    private void setFieldValue(Object object, String fieldName, Object value) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(object, value);
        } catch (Exception e) {
    
        }
    }

    private Double mean(List<Double> values) {
        return round(values.stream().mapToDouble(Double::doubleValue).average().orElse(0));
    }

    private Double getNewValue(double oldValue, double eq, double newEq) {
        return round((oldValue * newEq) / eq);
    }

    private Double getPercentage(double actualValue, double previousValue) {
        return previousValue != 0 ? round((actualValue - previousValue) / previousValue * 100) : null;
    }

    private Double applyCagr(double initialValue, double cagr, int years) {
        double finalValue = initialValue * Math.pow(1 + cagr / 100, years);
        return round(finalValue);
    }

    private Double calculateCagr(double initialValue, double finalValue, int years) {
        double cagr = Math.pow(finalValue / initialValue, 1.0 / years) - 1;
        return round(cagr * 100);
    }

    private Double round(double value) {
        return Math.round(value * 100.0) / 100.0;
    }
}
