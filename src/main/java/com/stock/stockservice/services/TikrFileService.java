package com.stock.stockservice.services;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.stockservice.models.Price;
import com.stock.stockservice.models.Quarter;
import com.stock.stockservice.models.Stock;
import com.stock.stockservice.models.tikr.Fin;
import com.stock.stockservice.models.tikr.FinData;
import com.stock.stockservice.models.tikr.FinancialPeriod;
import com.stock.stockservice.models.tikr.PriceFull;
import com.stock.stockservice.models.tikr.QuarterField;
import com.stock.stockservice.utils.PricesUtils;
import com.stock.stockservice.utils.QuarterUtils;
import com.stock.stockservice.utils.Utils;

@Service
public class TikrFileService {

    private String path = "D:\\MisCosas\\Proyectos\\stockdb\\tikr".replaceAll("\\\\", "/");
    private String path2 = "G:\\Mi unidad\\StockDB";

    public List<Stock> readStockDbFiles(List<String> symbols) {

        List<File> directories = Arrays.asList(new File(path).listFiles(File::isDirectory));
        if(symbols != null) {
            directories = directories.stream().filter(d -> symbols.contains(d.getName())).toList();
        }
        return directories.stream().map(d -> processStockDirectory(d)).filter(s -> s != null).toList();
    }

    public void readStockFiles() {
        System.out.println(path2);
         List<File> directories = Arrays.asList(new File(path2).listFiles(File::isDirectory));
         System.out.println(directories);
         directories.forEach((d) -> {
            System.out.println(d.getName());
         });
    }

    public Stock processStockDirectory(File d) {
        List<File> files = Arrays.asList(d.listFiles());
        PriceFull price = null;
        Fin fin = null;
        for (File f : files) {
            switch(f.getName()) {
                case "price.json":
                    price = mapFileToPriceFull(f);
                break;
                case "fin.json":
                    fin = mapFileToFin(f);
                break;
            }
        }
        if(fin != null && price != null) {
            System.out.println(d.getName());
            Stock stock = Stock.builder().symbol(d.getName().toUpperCase()).build();
            // processFin(fin, stock);
            processPrice(price, stock);
            return stock;
        }
        return null;
    }

    public Stock processFilePrice(String symbol, Stock stock) {
        List<File> directories = Arrays.asList(new File(path).listFiles(File::isDirectory));
        directories = directories.stream().filter(d -> symbol.equals(d.getName())).toList();
        List<File> files = Arrays.asList(directories.get(0).listFiles());
        PriceFull price = null;
        for (File f : files) {
            switch(f.getName()) {
                case "price.json":
                    price = mapFileToPriceFull(f);
                break;
            }
        }
        if(price != null) {
            // processFin(fin, stock);
            return processPrice(price, stock);
            // return stock;
        }
        return stock;
    }

    public PriceFull mapFileToPriceFull(File file) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(file, PriceFull.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Fin mapFileToFin(File file) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(file, Fin.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Stock processPrice(PriceFull price, Stock stock) {
        List<Price> prices = price.getPrice().stream().map(p -> {
            return Price.builder()
            .date(Instant.parse(p.getD()))
            .open(getPriceValue(p.getO()))
            .high(getPriceValue(p.getH()))
            .low(getPriceValue(p.getL()))
            .close(getPriceValue(p.getC()))
            .volume(getPriceValue(p.getV()))
            .build();
        }).toList();
        stock.setPrices(prices);
        stock.setPrices(PricesUtils.processPrices(stock));
        stock.setLastPrice(Utils.getLastPrice(stock.getPrices()));
        return stock;
    }

    public Double getPriceValue(String value) {
        return Double.parseDouble(value);
    }

    public void processFin(Fin fin, Stock stock) {
        List<Integer> financialCollectionIdsOmit = Arrays.asList(-2049177832, -2049177831, -2049177011);
        List<FinancialPeriod> financialPeriods = fin.getDates().stream().map(d -> {
            Integer financialPeriodId = d.getFinancialperiodid();
            return FinancialPeriod.builder()
                .date(d)
                .data(fin.data.stream().filter(v -> v.getFinancialperiodid().equals(financialPeriodId) && !financialCollectionIdsOmit.contains(v.getFinancialcollectionid())).toList())
                .std(fin.std.stream().filter(v -> v.getFinancialperiodid().equals(financialPeriodId)).toList())
                .rpt(fin.rpt.stream().filter(v -> v.getFinancialperiodid().equals(financialPeriodId)).toList())
                .build();
        }).toList();
        List<Quarter> quarters = financialPeriods.stream().map(fp -> {
            Quarter q = Quarter.builder()
                .fiscalDateEnd(Instant.parse(fp.getDate().getFiperiodenddate()))
                .earningDate(Instant.parse(fp.getDate().getFifilingdate()))
                .revenues(getValueFromFinData(QuarterField.REVENUES, fp.data))
                .costsOfGoodsSold(getValueFromFinData(QuarterField.COSTS_OF_GOOD_SOLD, fp.data))
                .grossProfit(getValueFromFinData(QuarterField.GROSS_PROFIT, fp.data))
                .operatingExpenses(getValueFromFinData(QuarterField.OPERATING_EXPENSES, fp.data))
                .operatingIncome(getValueFromFinData(QuarterField.OPERATING_INCOME, fp.data))
                .netIncome(getValueFromFinData(QuarterField.NET_INCOME, fp.data))
                .shares(getValueFromFinData(QuarterField.SHARES, fp.data))
                .totalCashAndShortInv(getValueFromFinData(QuarterField.TOTAL_CASH_AND_SHORT_INVESTMENTS, fp.data))
                .totalReceivable(getValueFromFinData(QuarterField.TOTAL_RECEIVABLES, fp.data))
                .totalCurrentAssets(getValueFromFinData(QuarterField.TOTAL_CURRENT_ASSETS, fp.data))
                .netPropertyPlantAndEquip(getValueFromFinData(QuarterField.NET_PROPERTY_PLANT_AND_EQUIPMENT, fp.data))
                .goodwill(getValueFromFinData(QuarterField.GOODWILL, fp.data))
                .totalAssets(getValueFromFinData(QuarterField.TOTAL_ASSETS, fp.data))
                .totalCurrentLiabilities(getValueFromFinData(QuarterField.TOTAL_CURRENT_LIABILITIES, fp.data))
                .longTermDebt(getValueFromFinData(QuarterField.LONG_TERM_DEBT, fp.data))
                .totalLiabilities(getValueFromFinData(QuarterField.TOTAL_LIABILITIES, fp.data))
                .totalEquity(getValueFromFinData(QuarterField.TOTAL_EQUITY, fp.data))
                .netDebt(getValueFromFinData(QuarterField.NET_DEBT, fp.data))
                .totalDeprecAmort(getValueFromFinData(QuarterField.TOTAL_DEPRECIATION_AND_AMORTIZATION, fp.data))
                .stockBasedCompensation(getValueFromFinData(QuarterField.STOCK_BASED_COMPENSATION, fp.data))
                .cashFromOperations(getValueFromFinData(QuarterField.CASH_FROM_OPERATIONS, fp.data))
                .capitalExpenditure(getValueFromFinData(QuarterField.CAPITAL_EXPENDITURE, fp.data))
                .cashFromInvesting(getValueFromFinData(QuarterField.CASH_FROM_INVESTING, fp.data))
                .cashFromFinancing(getValueFromFinData(QuarterField.CASH_FROM_FINANCING, fp.data))
                .netChangeinCash(getValueFromFinData(QuarterField.NET_CHANGE_IN_CASH, fp.data))
                .build();
                q = QuarterUtils.setCalculatedValues(q);
                return q;
        }).toList();
        stock.setQuarters(QuarterUtils.processQuarters(quarters));
        stock.setLastQuarter(Utils.getLastQuarter(stock.getQuarters()));
    }

    public Double getValueFromFinData(QuarterField field, List<FinData> dataList) {
        Integer fieldId = field.id();
        String value = dataList.stream().filter(d -> d.getDataitemid().equals(fieldId)).findFirst().orElse(FinData.builder().build()).getDataitemvalue();
        return value != null ? Double.parseDouble(value) : null;
    }
    


}
