package com.stock.stockservice.services;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

public class DateService {
      public static boolean isSameDay(Instant instant) {
        // Convert the given instant to the current system's default time zone
        LocalDate givenDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        // Get the current date in the default time zone
        LocalDate currentDate = LocalDate.now(ZoneId.systemDefault());
        // Compare the two dates
        return givenDate.equals(currentDate);
    }
}
