package com.stock.stockservice.utils;

import java.time.Instant;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.stock.stockservice.models.Price;
import com.stock.stockservice.models.Quarter;
import com.stock.stockservice.models.Stock;

@Service
public class Utils {

    public static Comparator<Quarter> quarterComparator() {
        return (q1, q2) -> q1.getFiscalDateEnd().compareTo(q2.getFiscalDateEnd());
    }

      public static Comparator<Price> priceComparator() {
        return (p1, p2) -> p1.getDate().compareTo(p2.getDate());
    }

    public static List<Quarter> sortQuarters(List<Quarter> quarters) {
        return quarters.stream().sorted(quarterComparator()).toList();
    }

    public static List<Price> sortPrices(List<Price> prices) {
        return prices.stream().sorted(priceComparator()).toList();
    }

    public static List<Quarter> filterQuartersByDate(List<Quarter> quarters, Instant date) {
        return quarters == null ? Collections.emptyList() : quarters.stream().filter(q -> q.getEarningDate().isBefore(date)).toList();
    }

    public static List<Quarter> sortAndFilterQuartersByDate(List<Quarter> quarters, Instant date) {
        return sortQuarters(filterQuartersByDate(quarters, date));
    }

    public static List<Price> filterPricesByDate(List<Price> prices, Instant date) {
        return prices == null ? Collections.emptyList() : sortPrices(prices.stream().filter(q -> q.getDate().isBefore(date)).toList());
    }

    public static Quarter getLastQuarter(Stock stock) {
        return getLastQuarter(stock.getQuarters());
    }

    public static Quarter getLastQuarter(List<Quarter> quarters) {
        return quarters != null && quarters.size() > 0 ? quarters.get(quarters.size() - 1) : null;
    }

    public static Price getLastPrice(List<Price> prices) {
        return prices != null && prices.size() > 0 ? prices.get(prices.size() - 1) : null;
    }

    public static Quarter getLastQuarterByDate(List<Quarter> quarters, Instant date) {
        return getLastQuarter(filterQuartersByDate(quarters, date));
    }

    public static List<Price> filterPricesByRange(List<Price> prices, Instant startDate, Instant endDate) {
        return sortPrices(prices.stream().filter(p -> p.getDate().isAfter(startDate) && (p.getDate().isBefore(endDate) || p.getDate().equals(endDate))).toList());
    }
    
}
