package com.stock.stockservice.utils;

import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;

import com.stock.stockservice.models.Quarter;
import com.stock.stockservice.models.QuarterData;
import com.stock.stockservice.models.QuarterDataStats;

public class QuarterUtils {
    

    public static Double getEps(Quarter q) {
    if (q.getNetIncome() != null && q.getShares() != null && q.getShares() != 0) {
        return MathUtils.round(q.getNetIncome() / q.getShares(), 2);
    }
    return null;
    }

    public static Double getFreeCashFlow(Quarter q) {
        if (q.getCapitalExpenditure() != null && q.getCashFromOperations() != null) {
            return q.getCapitalExpenditure() + q.getCashFromOperations();
        }
        return null;
    }

    public static Double getEbit(Quarter q) {
        if (q.getGrossProfit() != null && q.getOperatingExpenses() != null) {
            return q.getGrossProfit() + q.getOperatingExpenses();
        }
        return null;
    }

    public static Double getEbitda(Quarter q) {
        if (q.getOperatingIncome() != null && q.getTotalDeprecAmort() != null) {
            return q.getOperatingIncome() + q.getTotalDeprecAmort();
        }
        return null;
    }

    public static Double getCurrentRatio(Quarter q) {
        if (q.getTotalCurrentLiabilities() != null && q.getTotalCurrentAssets() != null && q.getTotalCurrentLiabilities() != 0) {
            return MathUtils.round(q.getTotalCurrentAssets() / q.getTotalCurrentLiabilities(), 2);
        }
        return null;
    }

    public static Double getRoi(Quarter q) {
        if (q.getNetIncome() != null && q.getTotalAssets() != null && q.getTotalLiabilities() != null && (q.getTotalAssets() - q.getTotalLiabilities()) != 0) {
            return MathUtils.round(q.getNetIncome() / (q.getTotalAssets() - q.getTotalLiabilities()) * 100, 2);
        }
        return null;
    }

    public static Double getRoe(Quarter q, Quarter qLastYear) {
        if (q.getNetIncome() != null && q.getTotalEquity() != null && qLastYear.getTotalEquity() != null && (q.getTotalEquity() + qLastYear.getTotalEquity() / 2) != 0) {
            return MathUtils.round(q.getNetIncome() / ((q.getTotalEquity() + qLastYear.getTotalEquity()) / 2) * 100, 2);
        }
        return null;
    }

    public static Double getRoa(Quarter q, Quarter qLastYear) {
        if (q.getNetIncome() != null && q.getTotalAssets() != null && qLastYear.getTotalAssets() != null && (q.getTotalAssets() + qLastYear.getTotalAssets() / 2) != 0) {
            return MathUtils.round(q.getNetIncome() / ((q.getTotalAssets() + qLastYear.getTotalAssets()) / 2) * 100, 2);
        }
        return null;
    }

    public static Quarter setCalculatedValues(Quarter q) {
        q.setEps(getEps(q));
        q.setFreeCashFlow(getFreeCashFlow(q));
        q.setEbit(getEbit(q));
        q.setEbitda(getEbitda(q));
        q.setCurrentRatio(getCurrentRatio(q));
        q.setRoi(getRoi(q));
        q.setGrossProfitMargin(getMargin(q, q.getGrossProfit()));
        q.setOperatingIncomeMargin(getMargin(q, q.getOperatingIncome()));
        q.setNetIncomeMargin(getMargin(q, q.getNetIncome()));
        q.setEbitMargin(getMargin(q, q.getEbit()));
        q.setEbitdaMargin(getMargin(q, q.getEbitda()));
        q.setFreeCashFlowMargin(getMargin(q, q.getFreeCashFlow()));
        q.setRevenuesData(getInitialQuarterData(q, q.getRevenues()));
        q.setGrossProfitData(getInitialQuarterData(q, q.getGrossProfit()));
        q.setOperatingIncomeData(getInitialQuarterData(q, q.getOperatingIncome()));
        q.setNetIncomeData(getInitialQuarterData(q, q.getNetIncome()));
        q.setEpsData(getInitialQuarterData(q, q.getEps()));
        q.setEbitdaData(getInitialQuarterData(q, q.getEbitda()));
        q.setFreeCashFlowData(getInitialQuarterData(q, q.getFreeCashFlow()));
        q.setSharesData(getInitialQuarterData(q, q.getShares()));
        return q;
    }

    public static List<Quarter> processQuarters(List<Quarter> quarters) {
        quarters = Utils.sortQuarters(quarters);
        List<Quarter> ltmQuarters = null;
        List<Quarter> last3YQuarters = null;
        List<Quarter> last5YQuarters = null;
        List<Quarter> last10YQuarters = null;
        List<Quarter> fullQuarters = null;
        Quarter qLastYear = null;
        Quarter q;
        for (int i = 0; i < quarters.size(); i++) {
            q = quarters.get(i);
            fullQuarters = quarters.subList(0, i + 1);
            if (i >= 3) {
                ltmQuarters = quarters.subList(i - 3, i + 1);
                if (i >= 4) {
                    qLastYear = quarters.get(i - 4);
                    if (i >= 11) {
                        last3YQuarters = quarters.subList(i - 11, i + 1);
                        if (i >= 19) {
                            last5YQuarters = quarters.subList(i - 19, i + 1);
                            if (i >= 38) {
                                last10YQuarters = quarters.subList(i - 38, i + 1);
                            }
                        }
                    }
                }
            }
            q.setRevenuesData(setQuarterData(q, q.getRevenuesData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getRevenuesData()));
            q.setGrossProfitData(setQuarterData(q, q.getGrossProfitData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getGrossProfitData()));
            q.setOperatingIncomeData(setQuarterData(q, q.getOperatingIncomeData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getOperatingIncomeData()));
            q.setNetIncomeData(setQuarterData(q, q.getNetIncomeData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getNetIncomeData()));
            q.setEpsData(setQuarterData(q, q.getEpsData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getEpsData()));
            q.setEbitdaData(setQuarterData(q, q.getEbitdaData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getEbitdaData()));
            q.setFreeCashFlowData(setQuarterData(q, q.getFreeCashFlowData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getFreeCashFlowData()));
            q.setSharesData(setQuarterData(q, q.getSharesData(), qLastYear, ltmQuarters, last3YQuarters, last5YQuarters, last10YQuarters, fullQuarters, v -> v.getSharesData()));
        }
        return quarters;
    }

    public static QuarterData getInitialQuarterData(Quarter q, Double value) {
        return QuarterData.builder()
            .value(value)
            .perShare(getPerShare(q, value))
            .build();
    }

     public static QuarterData setQuarterData(Quarter q, QuarterData qData, Quarter qLastYear, List<Quarter> ltmQuarters, List<Quarter> last3YQuarters, List<Quarter> last5YQuarters, List<Quarter> last10YQuarters, List<Quarter> fullQuarters, Function<Quarter, QuarterData> f) {
        if(ltmQuarters != null) {
            List<QuarterData> qDataList = getQuarterDataList(ltmQuarters, f);
            qData.setLtm(sum(qDataList, valueF()));
            qData.setLtmPerShare(getPerShare(q, qData.getLtm()));
            qData.setLastYStats(getStats(qDataList));

            if(qLastYear != null) {
                QuarterData qDataLastYear = f.apply(qLastYear);
                qData.setGrowth(MathUtils.growth(qData.getValue(), qDataLastYear.getValue()));
                qData.setPerShareGrowth(MathUtils.growth(qData.getPerShare(), qDataLastYear.getPerShare()));
                qData.setLtmGrowth(MathUtils.growth(qData.getLtm(), qDataLastYear.getLtm()));
                qData.setLtmPerShareGrowth(MathUtils.growth(qData.getLtmPerShare(), qDataLastYear.getLtmPerShare()));

                if(last3YQuarters != null) {
                    qDataList = getQuarterDataList(last3YQuarters, f);
                    qData.setLast3yStats(getStats(qDataList));

                    if(last5YQuarters != null) {
                        qDataList = getQuarterDataList(last5YQuarters, f);
                        qData.setLast5yStats(getStats(qDataList));

                        if(last10YQuarters != null) {
                            qDataList = getQuarterDataList(last10YQuarters, f);
                            qData.setLast5yStats(getStats(qDataList));
                        }   
                    }   
                }
            }
        }
        qData.setFullStats(getStats(getQuarterDataList(fullQuarters, f)));
        return qData;
    }

    public static List<QuarterData> getQuarterDataList(List<Quarter> qList, Function<Quarter, QuarterData> f) {
        return qList.stream().map(f).toList();
    }

    public static QuarterDataStats getStats(List<QuarterData> qList) {
        return QuarterDataStats
            .builder()
            .growthAvg(average(qList, q -> q.getGrowth()))
            .growthStd(std(qList, q -> q.getGrowth()))
            .perShareGrowthAvg(average(qList, q -> q.getPerShareGrowth()))
            .perShareGrowthStd(std(qList, q -> q.getPerShareGrowth()))
            .ltmGrowthAvg(average(qList, q -> q.getLtmGrowth()))
            .ltmGrowthStd(std(qList, q -> q.getLtmGrowth()))
            .ltmPerShareGrowthAvg(average(qList, q -> q.getLtmPerShareGrowth()))
            .ltmPerShareGrowthStd(std(qList, q -> q.getLtmPerShareGrowth()))
            .build();
    }

    public static ToDoubleFunction<QuarterData> valueF() {
        return q -> q.getValue();
    }

    public static Double getMargin(Quarter q, Double value) {
        return MathUtils.round(value / q.getRevenues() * 100, 2);
    }

    public static Double getPerShare(Quarter q, Double value) {
        return MathUtils.round(value / q.getShares(), 2);
    }

    public static Double sum(List<QuarterData> qDataList, ToDoubleFunction<QuarterData> f) {
        return qDataList.stream().mapToDouble(f).sum();
    }

    public static Double average(List<QuarterData> qDataList, Function<QuarterData, Double> f) {
        List<Double> values = qDataList.stream().map(f).toList();
        return MathUtils.average(values);
    }

        public static Double std(List<QuarterData> qDataList, Function<QuarterData, Double> f) {
        List<Double> values = qDataList.stream().map(f).toList();
        return MathUtils.std(values);
    }

    public static Double min(List<QuarterData> qDataList, ToDoubleFunction<QuarterData> f) {
        return qDataList.stream().filter(v -> v != null).mapToDouble(f).min().orElseGet(null);
    }

    public static Double max(List<QuarterData> qDataList, ToDoubleFunction<QuarterData> f) {
        return qDataList.stream().filter(v -> v != null).mapToDouble(f).max().orElseGet(null);
    }
}
