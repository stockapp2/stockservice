package com.stock.stockservice.utils;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import com.stock.stockservice.models.Price;
import com.stock.stockservice.models.PriceData;
import com.stock.stockservice.models.PriceDataStats;
import com.stock.stockservice.models.Quarter;
import com.stock.stockservice.models.Stock;
import com.stock.stockservice.models.ValuePriceData;

public class PricesUtils {


    public static List<Price> processPrices(Stock s) {
        List<Price> prices = Utils.sortPrices(s.getPrices());
        Quarter firstQuarter = s.getQuarters().get(0);
        prices = Utils.filterPricesByRange(prices, firstQuarter.getEarningDate().minus(100, ChronoUnit.DAYS), Instant.now());
        List<Price> fullPrices = null;
        for (int i = 0; i < prices.size(); i++) {
            PricesUtils.processPrice(prices.get(i), s, firstQuarter, fullPrices, i);
        }
        return prices;
    }

    public static Price buildAndProcessPrice(Double close, Instant date, Stock s) {
        Price price = Price.builder()
            .date(date)
            .close(close)
            .build();
        return processPrice(price, s, null, null, null);
    }

    private static Price processPrice(Price p, Stock s, Quarter firstQuarter, List<Price> prices, Integer index) {
        if (firstQuarter == null) {
            firstQuarter = s.getQuarters().get(0);
        }
        if (prices == null) {
            prices = Utils.sortPrices(s.getPrices());
        }
        if (index == null) {
            index = prices.size();
        }
        Quarter q = Utils.getLastQuarterByDate(s.getQuarters(), p.getDate());
        if (q != null) {
            p.setMarketCap(getMarketCap(p, q));
            p.setEv(getEv(p, q));
            p.setPer(getPer(p, q));
            
            List<Price> fullPrices = prices.subList(0, index + 1);
            List<Price> lastQuarterPrices = null;
            List<Price> lastYearPrices = null;
            List<Price> last3YearPrices = null;
            List<Price> last5YearPrices = null;
            List<Price> last10YearPrices = null;

            if (index >= 60) {
                lastQuarterPrices = prices.subList(index - 60, index + 1);
                if (index >= 240) {
                    lastYearPrices = prices.subList(index - 240, index + 1);
                    if (index >= 720) {
                        last3YearPrices = prices.subList(index - 720, index + 1);
                        if (index >= 1200) {
                            last5YearPrices = prices.subList(index - 1200, index + 1);
                            if (index >= 2400) {
                                last10YearPrices = prices.subList(index - 2400, index + 1);
                            }
                        }
                    }
                }
            }

            p.setEvData(getInitialPriceData(p.getEv(), p, firstQuarter));
            p.setEvData(setPriceData(p.getEvData(), p, q, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, v -> v.getEvData()));

            PriceData volumePriceData = getInitialPriceData(p.getVolume(), p, firstQuarter);
            p.setVolumeData(getInitialValuePriceData(p.getVolume(), volumePriceData));
            p.setVolumeData(setValuePriceData(p.getVolumeData(), volumePriceData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, v -> v.getVolumeData()));
        }
        return p;
    }


    public static PriceData getInitialPriceData(Double value, Price p, Quarter q) {
        return PriceData.builder()
                .value(value)
                .fcfYield(getFcfYield(p, q, value))
                .build();
    }

    public static PriceData setPriceData(PriceData pData, Price p, Quarter q, List<Price> lastQuarterPrices, List<Price> lastYearPrices, List<Price> last3YearPrices, List<Price> last5YearPrices, List<Price> last10YearPrices, List<Price> fullPrices, Function<Price, PriceData> f) {
        pData.setRevenuesLtm(getInitialValuePriceData(q.getRevenuesData().getLtm(), pData));
        pData.setGrossProfitLtm(getInitialValuePriceData(q.getGrossProfitData().getLtm(), pData));
        pData.setOperatingIncomeLtm(getInitialValuePriceData(q.getOperatingIncomeData().getLtm(), pData));
        pData.setNetIncomeLtm(getInitialValuePriceData(q.getNetIncomeData().getLtm(), pData));
        pData.setEpsLtm(getInitialValuePriceData(q.getEpsData().getLtm(), pData));
        pData.setEbitdaLtm(getInitialValuePriceData(q.getEbitdaData().getLtm(), pData));
        pData.setFreeCashFlowLtm(getInitialValuePriceData(q.getFreeCashFlowData().getLtm(), pData));
        pData.setRevenuesLtm(setValuePriceData(pData.getRevenuesLtm(), pData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, fun(f, v -> v.getRevenuesLtm())));
        pData.setGrossProfitLtm(setValuePriceData(pData.getGrossProfitLtm(), pData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, fun(f, v -> v.getGrossProfitLtm())));
        pData.setOperatingIncomeLtm(setValuePriceData(pData.getOperatingIncomeLtm(), pData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, fun(f, v -> v.getOperatingIncomeLtm())));
        pData.setNetIncomeLtm(setValuePriceData(pData.getNetIncomeLtm(), pData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, fun(f, v -> v.getNetIncomeLtm())));
        pData.setEpsLtm(setValuePriceData(pData.getEpsLtm(), pData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, fun(f, v -> v.getEpsLtm())));
        pData.setEbitdaLtm(setValuePriceData(pData.getEbitdaLtm(), pData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, fun(f, v -> v.getEbitdaLtm())));
        pData.setFreeCashFlowLtm(setValuePriceData(pData.getFreeCashFlowLtm(), pData, lastQuarterPrices, lastYearPrices, last3YearPrices, last5YearPrices, last10YearPrices, fullPrices, fun(f, v -> v.getFreeCashFlowLtm())));
        return pData;
    }

    public static Function<Price, ValuePriceData> fun(Function<Price, PriceData> f, Function<PriceData, ValuePriceData> f2) {
        return p -> {
            PriceData pData = f.apply(p);
            return pData != null ? f2.apply(pData) : null;
        };
    }

    public static ValuePriceData getInitialValuePriceData(Double value, PriceData priceData) {
        if (value == null || priceData == null || priceData.getValue() == null) {
            return null;
        }
        return ValuePriceData.builder().value(MathUtils.round(priceData.getValue() / value, 2)).build();
    }
    
    public static ValuePriceData setValuePriceData(ValuePriceData vPriceData, PriceData priceData, List<Price> lastQuarterPrices, List<Price> lastYearPrices, List<Price> last3YearPrices, List<Price> last5YearPrices, List<Price> last10YearPrices, List<Price> fullPrices, Function<Price, ValuePriceData> f) {
        if(lastQuarterPrices != null && vPriceData != null) {
            List<ValuePriceData> pDataList = getValuePriceDataList(lastQuarterPrices, f);
            vPriceData.setLastQStats(getStats(pDataList));

            if(lastYearPrices != null) {
                pDataList = getValuePriceDataList(lastYearPrices, f);
                vPriceData.setLastYStats(getStats(pDataList));

                if(last3YearPrices != null) {
                    pDataList = getValuePriceDataList(last3YearPrices, f);
                    vPriceData.setLast3yStats(getStats(pDataList));

                    if(last5YearPrices != null) {
                        pDataList = getValuePriceDataList(last5YearPrices, f);
                        vPriceData.setLast5yStats(getStats(pDataList));

                        if(last10YearPrices != null) {
                            pDataList = getValuePriceDataList(last10YearPrices, f);
                            vPriceData.setLast10yStats(getStats(pDataList));
                        }
                    }
                }
            }
        }
        return vPriceData;
    }


    public static PriceDataStats getStats(List<ValuePriceData> pList) {
        return PriceDataStats
            .builder()
            .avg(average(pList))
            .max(max(pList))
            .min(min(pList))
            .std(std(pList))
            .build();
    }

    public static List<PriceData> getPriceDataList(List<Price> pList, Function<Price, PriceData> f) {
        return pList.stream().map(f).filter(Objects::nonNull).toList();
    }

    public static List<ValuePriceData> getValuePriceDataList(List<Price> pList, Function<Price, ValuePriceData> f) {
        return pList.stream().map(f).filter(Objects::nonNull).toList();
    }

    public static List<Double> getValuesFromValuePriceDataList(List<ValuePriceData> pDataList) {
        return pDataList.stream().map(ValuePriceData::getValue).toList();
    }

    public static Double average(List<ValuePriceData> pDataList) {
        return MathUtils.average(getValuesFromValuePriceDataList(pDataList));
    }

    public static Double max(List<ValuePriceData> pDataList) {
         return MathUtils.max(getValuesFromValuePriceDataList(pDataList));
    }

    public static Double min(List<ValuePriceData> pDataList) {
         return MathUtils.min(getValuesFromValuePriceDataList(pDataList));
    }

    public static Double std(List<ValuePriceData> pDataList) {
         return MathUtils.std(getValuesFromValuePriceDataList(pDataList));
    }

    public static Double getMarketCap(Price p, Quarter q) {
        if (p == null || q == null || q.getShares() == null || p.getClose() == null) {
            return null;
        }
        return q.getShares() * p.getClose();
    }

    public static Double getEv(Price p, Quarter q) {
        if (p == null || q == null || p.getMarketCap() == null || q.getNetDebt() == null) {
            return null;
        }
        return p.getMarketCap() + q.getNetDebt();
    }

    public static Double getPer(Price p, Quarter q) {
        if (p == null || q == null || p.getClose() == null || q.getEpsData() == null || q.getEpsData().getLtm() == null) {
            return null;
        }
        return MathUtils.round(p.getClose() / q.getEpsData().getLtm(), 2);
    }

    public static Double getFcfYield(Price p, Quarter q, Double value) {
        if (q == null || q.getFreeCashFlow() == null || value == null) {
            return null;
        }
        return MathUtils.round(q.getFreeCashFlow() / value, 2);
    }

    
}
