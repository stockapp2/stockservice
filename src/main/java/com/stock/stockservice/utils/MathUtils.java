package com.stock.stockservice.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;
import java.util.function.ToDoubleFunction;
import java.util.stream.DoubleStream;

public class MathUtils {

    public static Double round(Double value, int places) {
        try {
            if (places < 0) throw new IllegalArgumentException();
            BigDecimal bd = BigDecimal.valueOf(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        } catch (Exception e) {
            return 0.0;
        }
    }

    public static Double growth(Double actualValue, Double previousValue) {
        return  actualValue != null && previousValue != null ? round((actualValue - previousValue) / previousValue * 100, 2) : null;
    }

     public static Double std(List<Double> values) {
        List<Double> filteredValues = values.stream()
                                            .filter(Objects::nonNull)
                                            .toList();
        
        Double mean = average(filteredValues);
        Double std = Math.sqrt(filteredValues.stream()
                                             .mapToDouble(v -> Math.pow(v - mean, 2))
                                             .sum() / filteredValues.size());

        return round(std, 2);
    }

    public static Double average(List<Double> values) {
        return calculate(values, stream -> stream.average().orElse(Double.NaN));
    }

    public static Double min(List<Double> values) {
        return calculate(values, stream -> stream.min().orElse(Double.NaN));
    }

    public static Double max(List<Double> values) {
        return calculate(values, stream -> stream.max().orElse(Double.NaN));
    }

    public static Double calculate(List<Double> values, ToDoubleFunction<DoubleStream> operation) {
        List<Double> validValues = getValidValues(values);
        if (validValues.isEmpty()) {
            return null;
        }
        Double value = operation.applyAsDouble(validValues.stream().mapToDouble(v -> v));
        return value.isNaN() ? null : round(value, 2);
    }

    public static List<Double> getValidValues(List<Double> values) {
        return values.stream().filter(Objects::nonNull).toList();
    }
    
}
