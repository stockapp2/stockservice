package com.stock.stockservice;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.stock.stockservice.services.GoogleDocsStockService;
import com.stock.stockservice.services.StockPriceService;
import com.stock.stockservice.services.StockService;
import com.stock.stockservice.services.TikrFileService;

import jakarta.annotation.PostConstruct;

@Configuration
@EnableAutoConfiguration
@EnableMongoRepositories
@ComponentScan({"com.stock.stockservice"})
public class StockServiceApp {

	@Autowired
	StockService stockService;

	@Autowired
	StockPriceService stockPriceService;

	@Autowired
	GoogleDocsStockService googleDocsStockService;

	@Autowired
    TikrFileService tikrFileService;
	
	public static void main(String[] args) {
		SpringApplication.run(StockServiceApp.class, args);
	}

	@PostConstruct
	public void saveInfo() {
		this.stockPriceService.cacheStockPrice();
 		List<String> symbols = Arrays.asList("GOOGL");
		stockService.processAndSaveStocks(symbols);
		// tikrFileService.readStockFiles();
	}

}
