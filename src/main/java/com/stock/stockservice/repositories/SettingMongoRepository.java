package com.stock.stockservice.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.stock.stockservice.models.Setting;


@Repository
public interface SettingMongoRepository extends MongoRepository<Setting, String> {

}
