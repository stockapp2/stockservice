package com.stock.stockservice.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.stock.stockservice.models.Stock;


@Repository
public interface StockMongoRepository extends MongoRepository<Stock, String> {

    Stock findBySymbol(String symbol);
    List<Stock> findBySymbolIn(List<String> symbols);

    @Query(value = "{ 'symbol' : { $in: ?0 } }", fields = "{ 'quarters' : 0, 'prices' : 0 }")
    List<Stock> findBySymbolList(List<String> symbols);

    @Query(value = "{}", fields = "{ 'quarters' : 0, 'prices' : 0 }")
    List<Stock> findAllWithoutQuartersAndPrices();
}
