package com.stock.stockservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.stock.stockservice.models.StockPrices;
import com.stock.stockservice.services.StockPriceService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/stockprices")
public class StockPriceController {

    @Autowired
    private StockPriceService stockPriceService;

    @GetMapping("")
    public Mono<StockPrices> getStockPrices() {
        return stockPriceService.getStockPrices();
    }

    @PostMapping("")
    public Mono<StockPrices> saveStockPrices(@RequestBody StockPrices stockPrices) {
        return Mono.just(stockPriceService.saveStockPrices(stockPrices));
    }
}
