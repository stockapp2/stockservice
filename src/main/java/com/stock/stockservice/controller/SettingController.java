package com.stock.stockservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.stock.stockservice.models.Setting;
import com.stock.stockservice.services.SettingService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/setting")
public class SettingController {

    @Autowired
    private SettingService settingService;

    @GetMapping("")
    public Mono<Setting> getSettings() {
        return settingService.getSettings();
    }

    @PostMapping("")
    public Mono<Setting> saveSettings(@RequestBody Setting setting) {
        return Mono.just(settingService.saveSettings(setting));
    }
}
