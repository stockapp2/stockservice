package com.stock.stockservice.controller;

import java.time.Instant;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stock.stockservice.models.Stock;
import com.stock.stockservice.services.StockService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController 
@RequestMapping("/v1/stock")
public class StockController {

    @Autowired
    StockService stockService;

    @GetMapping("")
    public Flux<Stock> getStocks(@RequestParam(required = false) List<String> symbols, @RequestParam String dateUnix) {
        Instant date = Instant.ofEpochMilli(Long.parseLong(dateUnix));
        return Flux.fromIterable(stockService.getStocks(symbols == null ? List.of() : symbols, date));
    }

    @GetMapping("{symbol}")
    public Mono<Stock> getStock(@PathVariable String symbol, @RequestParam String dateUnix) {
        Instant date = Instant.ofEpochMilli(Long.parseLong(dateUnix));
        return Mono.just(stockService.getStock(symbol, date));
    }

    @PostMapping("")
    public Flux<Stock> processStocks(@RequestBody List<String> symbols) {
        return Flux.fromIterable(stockService.processAndSaveStocks(symbols));
    }
    
}
